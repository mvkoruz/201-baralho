package elementos;

public class Carta {
	private Naipe naipe;
	private Rank rank;
	
	public Carta(Rank rank, Naipe naipe) {
		this.naipe = naipe;
		this.rank = rank;
	}
	
	public Naipe getNaipe() {
		return naipe;
	}
	
	public Rank getRank() {
		return rank;
	}
	
	public String toString() {
		return rank.toString() + " de " + naipe.toString();
	}
}
